import numpy as np
import matplotlib.pyplot as plt


signal_file = open("signal.csv", "r")
signal = [item.replace('"', '')[:-1].split(",") for item in signal_file.readlines()[1:]]
signal = [[int(item[0]), int(item[1])] for item in signal if item[1] != "NA"]
signal.sort(key=lambda x: x[0])
signal = np.array([item[1] for item in signal])
signal_file.close()

# signal = signal[15000:19000]

signal_jump_threshold = 50
moving_average_length = 51
moving_average_passes = 3


def moving_average(x, w):
    processed = []
    for i in range(w - 1):
        processed.append(0)
    for i in range(w - 1, len(x)):
        values = x[i + 1 - w: i + 1]
        diffs = calc_diff(values)
        if np.any([abs(item) > signal_jump_threshold for item in diffs]):
            processed.append(values[-1])
        else:
            processed.append(np.mean(values))
    return processed


def calc_diff(signal):
    diff = []
    for i in range(1, len(signal)):
        diff.append(signal[i] - signal[i - 1])
    return diff


processed_signal = signal
for i in range(moving_average_passes):
    processed_signal = moving_average(processed_signal, moving_average_length)


plt.plot(signal)
plt.plot(processed_signal)
plt.savefig('result.png')
